package by.ls.ce.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickSort {

    public static void sort() {
        List<String> lines = new ArrayList<String>();
        lines.add("1asd");
        lines.add("fasd asfsaw");
        lines.add("s");
        lines.add("tas sad");
        lines.add("asd");
        for(String s : quickSort(lines)) {
            System.out.println(s);
        }
    }
    
    private static List<String> quickSort(List<String> lines) {
        List<String> result = new ArrayList<String>();
        if(lines.size() == 0 || lines.size() == 1) {
            return lines;
        }
        if(lines.size() == 2) {
            if(lines.get(0).length() > lines.get(1).length()) {
                Collections.reverse(lines);
            }
            return lines;
        }
        int pivot = lines.get(0).length();
        List<String> left = new ArrayList<String>();
        List<String> mid = new ArrayList<String>();
        List<String> right = new ArrayList<String>();
        for(String s : lines) {
            if(s.length() < pivot) {
                left.add(s);
            } else if (s.length() > pivot){
                right.add(s);
            } else {
                mid.add(s);
            }
        }
        result.addAll(quickSort(left));
        result.addAll(mid);
        result.addAll(quickSort(right));
        return result;
    }
}
