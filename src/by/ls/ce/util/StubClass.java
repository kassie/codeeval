package by.ls.ce.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * On submit: <br>
 * remove package declaration<br>
 * rename class to 'Main'<br>
 * replace file path with <code>args[0]</code>
 */
public class StubClass {

    public void solve(BufferedReader bufferedReader, PrintStream out) throws IOException {

        String line;
        while ((line = bufferedReader.readLine()) != null) {

        }
    }

    public static void main(String[] args) throws IOException {
        new StubClass().solve(new BufferedReader(new FileReader("samples/levenshtein.txt")), System.out);
    }
}
