package by.ls.ce;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import by.ls.ce.problem.hard.BusNetwork;

public class Main {

    private static final String INPUT = "samples/bus_network.txt";
    private static final String OUTPUT = "samples/output.txt";

    public static void main(String[] args) throws Exception {
        // long start = System.currentTimeMillis();
        // OutputStream os = new FileOutputStream(OUTPUT);
        new BusNetwork().solve(getReader(), System.out);
        // new Labyrinth().solve(getReader(), os);
        // System.out.println(System.currentTimeMillis() - start);
    }

    public static BufferedReader getReader() throws Exception {
        File file = new File(INPUT);
        return new BufferedReader(new FileReader(file));
    }
}
