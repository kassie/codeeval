package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Sudoku {

    public void solve(BufferedReader in, OutputStream os) throws IOException {
        for (Boolean b : calculate(in)) {
            os.write((b.toString()
                    .replace(b.toString().charAt(0), Character.toUpperCase(b.toString().charAt(0))
                    ) + "\n").getBytes());
        }
    }

    private List<Boolean> calculate(BufferedReader in) throws IOException {
        List<Boolean> result = new ArrayList<>();
        String line;
        while ((line = in.readLine()) != null) {
            int dimension = Integer.parseInt(line.split(";")[0]);
            int[][] field = parseGrid(line, dimension);
            result.add(checkRows(field)
                    & checkCols(dimension, field)
                    & checkSquares(dimension, field));
        }
        return result;
    }

    private boolean checkSquares(int dimension, int[][] field) {
        int sqrt = (int) Math.sqrt(dimension);
        for (int outerI = 0; outerI < sqrt; outerI++) {
            for (int outerJ = 0; outerJ < sqrt; outerJ++) {
                Set<Integer> testSet = new HashSet<>();
                for (int innerI = outerI * sqrt; innerI < sqrt * outerI + sqrt; innerI++) {
                    for (int innerJ = outerJ * sqrt; innerJ < sqrt * outerJ + sqrt; innerJ++) {
                        if (!testSet.add(field[innerI][innerJ])) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean checkCols(int dimension, int[][] field) {
        for (int j = 0; j < dimension; j++) {
            Set<Integer> testSet = new HashSet<>();
            for (int i = 0; i < dimension; i++) {
                if (!testSet.add(field[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkRows(int[][] field) {
        for (int[] row : field) {
            Set<Integer> testSet = new HashSet<>();
            for (int i : row) {
                if (!testSet.add(i)) {
                    return false;
                }
            }
        }
        return true;
    }

    private int[][] parseGrid(String line, int dimension) {
        int[][] field = new int[dimension][dimension];
        String[] numbers = line.split(";")[1].split(",");
        int counter = 0;
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                field[i][j] = Integer.parseInt(numbers[counter++]);
            }
        }
        return field;
    }
}
