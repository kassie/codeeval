package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class StackSolver {

    static final String DELIMITER = " ";

    public void solve(BufferedReader in, OutputStream os) throws IOException {
        List<Stack> stacks = new ArrayList<>();
        String line;
        while ((line = in.readLine()) != null) {
            stacks.add(parseStack(line));
        }
        for (Stack stack : stacks) {
            os.write((stack.printStack() + "\n").getBytes());
        }
    }

    private Stack parseStack(String line) {
        Stack stack = new Stack();
        for (String s : line.split(DELIMITER)) {
            stack.push(Integer.parseInt(s));
        }
        return stack;
    }

}

class Stack {

    private List<Integer> stack = new ArrayList<>();

    public int pop() throws RuntimeException {
        if (stack.size() < 1) {
            throw new RuntimeException("Stack is empty");
        }
        int result = stack.get(stack.size() - 1);
        stack.remove(stack.size() - 1);
        return result;
    }

    public boolean push(int i) {
        stack.add(i);
        return true;
    }

    public String printStack() {
        StringBuilder result = new StringBuilder();
        try {
            int stackSize = stack.size();
            for (int i = 0; i < stackSize; i++) {
                int value = pop();
                if (i % 2 == 0) {
                    result.append(value);
                    result.append(StackSolver.DELIMITER);
                }
            }
        } catch (RuntimeException e) {
        }
        return result.substring(0, result.lastIndexOf(StackSolver.DELIMITER));
    }
}
