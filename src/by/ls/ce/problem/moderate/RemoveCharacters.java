package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class RemoveCharacters {

    private void solve(BufferedReader bufferedReader, PrintStream out) throws IOException {
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] params = line.split(",");
            String modifiedLine = params[0];
            String[] split = params[1].trim().split("");
            for (int i = 1; i < split.length; i++) {
                modifiedLine = modifiedLine.replaceAll(split[i], "");
            }
            out.write((modifiedLine + "\n").getBytes());
        }
    }

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        new RemoveCharacters().solve(new BufferedReader(new FileReader("samples/remove_character.txt")), System.out);
        System.out.println(System.currentTimeMillis() - start);
    }

}
