package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DecimalToBinary {

    private void solve(BufferedReader in, OutputStream os) throws IOException {
        String line = null;
        while ((line = in.readLine()) != null) {
            os.write(getBitString(line).getBytes());
        }
    }

    private String getBitString(String line) {
        List<Integer> bits = getBits(Integer.parseInt(line));
        StringBuilder sb = new StringBuilder();
        for (int i = bits.size() - 1; i >= 0; i--) {
            sb.append(bits.get(i));
        }
        sb.append("\n");
        return sb.toString();
    }

    private List<Integer> getBits(int number) {
        List<Integer> bits = new ArrayList<>();
        if (number == 0) {
            bits.add(0);
        } else {
            while (number != 0) {
                bits.add(number % 2);
                number = number / 2;
            }
        }
        return bits;
    }

    public static void main(String[] args) throws IOException {
        long start = System.nanoTime();
        new DecimalToBinary().solve(new BufferedReader(new FileReader("samples/decimal_to_binary.txt")), System.out);
        System.out.println(System.nanoTime() - start);
    }
}
