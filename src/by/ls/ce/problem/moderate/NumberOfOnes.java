package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

public class NumberOfOnes {

    private void solve(BufferedReader in, OutputStream os) throws IOException {
        String line = null;
        while((line = in.readLine()) != null) {
            int number = Integer.parseInt(line);
            os.write(("" + oneEncounters(number) + "\n").getBytes());
        }
    }

    private int oneEncounters(int number) {
        int oneEncounter = 0;
        while(number != 0) {
            if(number % 2 == 1) {
                oneEncounter++;
            }
            number = number / 2;
        }
        return oneEncounter;
    }

    public static void main(String[] args) throws IOException {
        long start = System.nanoTime();
        new NumberOfOnes().solve(new BufferedReader(new FileReader("samples/number_of_ones.txt")), System.out);
        System.out.println(System.nanoTime() - start);
    }

}
