package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LongestLineProblem {
    
    public static void lngstLines(BufferedReader in) throws Exception {
        String line;
        int number = 0;
        boolean firstLine = true;
        List<String> lines = new ArrayList<String>();
        while((line = in.readLine()) != null) {
            if(firstLine) {
                number = Integer.parseInt(line);
                firstLine = false;
            } else {
                lines.add(line);
            }
        }
        List<String> sortedLines = quickSortStringByLength(lines);
        for(int i = 0; i < number; i++) {
            System.out.println(sortedLines.get(i));
        }
    }
    
    private static List<String> quickSortStringByLength(List<String> lines) {
        List<String> result = new ArrayList<String>();
        if(lines.size() == 0 || lines.size() == 1) {
            return lines;
        }
        if(lines.size() == 2) {
            if(lines.get(0).length() < lines.get(1).length()) {
                Collections.reverse(lines);
            }
            return lines;
        }
        int pivot = lines.get(0).length();
        List<String> left = new ArrayList<String>();
        List<String> mid = new ArrayList<String>();
        List<String> right = new ArrayList<String>();
        for(String s : lines) {
            if(s.length() > pivot) {
                left.add(s);
            } else if (s.length() < pivot){
                right.add(s);
            } else {
                mid.add(s);
            }
        }
        result.addAll(quickSortStringByLength(left));
        result.addAll(mid);
        result.addAll(quickSortStringByLength(right));
        return result;
    }
}
