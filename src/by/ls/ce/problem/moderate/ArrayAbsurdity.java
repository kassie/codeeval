package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

public class ArrayAbsurdity {

    private void solve(BufferedReader in, OutputStream os) throws IOException {
        String line = null;
        while ((line = in.readLine()) != null) {
            if (!line.isEmpty()) {
                String[] params = line.split(";");
                os.write((String.valueOf(findDuplicate(params)) + "\n").getBytes());
            }
        }
    }

    private int findDuplicate(String[] params) {
        // int n = Integer.parseInt(params[0]);
        Set<Integer> unique = new HashSet<>();
        for (String s : params[1].split(",")) {
            int x = Integer.parseInt(s);
            if (!unique.add(x)) {
                return x;
            }
        }
        throw new RuntimeException("no duplicate entry!");
    }

    public static void main(String[] args) throws IOException {
        long start = System.nanoTime();
        new ArrayAbsurdity().solve(new BufferedReader(new FileReader("samples/array_absurdity.txt")), System.out);
        System.out.println(System.nanoTime() - start);
    }
}
