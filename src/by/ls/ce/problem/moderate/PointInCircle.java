package by.ls.ce.problem.moderate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

public class PointInCircle {
    private void solve(BufferedReader in, OutputStream os) throws IOException {
        String line = null;
        while ((line = in.readLine()) != null) {
            String[] params = line.split(";");

            String[] centerCoordinates = removeParentheses(params[0].split(":")[1]).split(",");
            double centerX = Double.parseDouble(centerCoordinates[0]);
            double centerY = Double.parseDouble(centerCoordinates[1]);

            double radius = Double.parseDouble(params[1].split(":")[1]);

            String[] pointCoordinates = removeParentheses(params[2].split(":")[1]).split(",");
            double x = Double.parseDouble(pointCoordinates[0]);
            double y = Double.parseDouble(pointCoordinates[1]);

            double pointDest = Math.sqrt((x - centerX) * (x - centerX) + (y - centerY) * (y - centerY));
            os.write((String.valueOf(pointDest < radius) + "\n").getBytes());
        }
    }

    private String removeParentheses(String line) {
        return line.replaceAll("[()]", "");
    }

    public static void main(String[] args) throws IOException {
        long start = System.nanoTime();
        new PointInCircle().solve(new BufferedReader(new FileReader("samples/points_in_circle.txt")), System.out);
        System.out.println(System.nanoTime() - start);
    }
}
