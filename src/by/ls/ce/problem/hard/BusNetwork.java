package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// TODO not solved
public class BusNetwork {

    private Integer departure;
    private Integer arrival;
    private List<Route> busRoutes;

    public void solve(BufferedReader bufferedReader, PrintStream out) throws IOException {
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            parseInput(line);
            PathFinder pathFinder = new PathFinder();
            Map<Integer, Node> graph = pathFinder.buildGraph();
            List<Route> paths = pathFinder.findPaths();
            // System.out.println(paths);
            WayComparer wayComparer = new WayComparer();
            wayComparer.setRoutes(busRoutes);
            wayComparer.setWays(paths);
            wayComparer.solve();
            for (Entry<Integer, Node> entry : graph.entrySet()) {
                // System.out.println(entry.getValue());
            }

        }
    }

    // (1,7); R1=[1,2,3,4]; R2=[5,6,4]; R3=[9,6,7]; R4=[12,1,2,3,11,16,15,14,10,13,7]
    // (3,299); R1=[1,2,3,4]; R2=[6,7,19,12,4]; R3=[11,14,16,6]; R4=[24,299,42,6]
    // (3,4); R1=[1,2,3]; R2=[6,7,19,12,4]; R3=[11,14,16,6]

    private void parseInput(String line) throws IOException {
        String[] params = line.split(";");
        String[] routeParams = removeParentheses(params[0]).split(",");
        departure = Integer.parseInt(routeParams[0]);
        arrival = Integer.parseInt(routeParams[1]);
        parseRoutes(params);
    }

    private void parseRoutes(String[] params) {
        busRoutes = new ArrayList<>();
        for (int i = 1; i < params.length; i++) {
            List<Integer> stops = new ArrayList<>();
            for (String stop : removeParentheses((params[i].split("=")[1])).split(",")) {
                stops.add(Integer.parseInt(stop.trim()));
            }
            busRoutes.add(new Route(stops));
        }
    }

    private class PathFinder {

        private Map<Integer, Node> graph = new HashMap<>();
        private List<Route> paths = new ArrayList<>();

        public Map<Integer, Node> buildGraph() {
            Set<Integer> stops = new HashSet<>();
            for (Route route : busRoutes) {
                stops.addAll(route.getStops());
            }

            for (Integer stop : stops) {
                graph.put(stop, new Node(stop));
            }

            for (Entry<Integer, Node> entry : graph.entrySet()) {
                for (Route route : busRoutes) {
                    int index = route.getStops().indexOf(entry.getKey());
                    if (index >= 0) {
                        if (index == 0) {
                            entry.getValue().addSibling(graph.get(route.getStops().get(index + 1)));
                        } else if (index == route.getStops().size() - 1) {
                            entry.getValue().addSibling(graph.get(route.getStops().get(index - 1)));
                        } else {
                            entry.getValue().addSibling(graph.get(route.getStops().get(index - 1)));
                            entry.getValue().addSibling(graph.get(route.getStops().get(index + 1)));
                        }

                    }
                }
            }
            return graph;
        }

        public List<Route> findPaths() {
            checkSiblings(graph.get(departure), new Route(Arrays.asList(departure)));
            return paths;
        }

        private void checkSiblings(Node node, Route route) {
            for (Node sibl : node.getSiblings()) {
                if (route.getStops().contains(sibl.getValue())) {
                    continue;
                }
                Route newRoute = new Route(route.getStops());
                newRoute.addStop(sibl.getValue());
                if (sibl.getValue() == arrival) {
                    paths.add(newRoute);
                    continue;
                }
                checkSiblings(sibl, newRoute);
            }
        }
    }

    private String removeParentheses(String s) {
        return s.substring(1, s.length() - 1);
    }

    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        new BusNetwork().solve(new BufferedReader(new FileReader("samples/bus_network.txt")), System.out);
        System.out.println(System.currentTimeMillis() - start);
    }
}

class Route {
    private List<Integer> stops = new ArrayList<>();

    public Route(List<Integer> stops) {
        this.getStops().addAll(stops);
    }

    public Route() {
    }

    public int index(Integer i) {
        return this.stops.indexOf(i);
    }

    public List<Integer> getStops() {
        return stops;
    }

    public void addStop(Integer stop) {
        stops.add(stop);
    }

    @Override
    public String toString() {
        return this.stops.toString();
    }
}

class Node {

    private int value;
    private Set<Node> siblings = new HashSet<>();

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Set<Node> getSiblings() {
        return this.siblings;
    }

    public void addSiblings(List<Node> siblings) {
        this.siblings.addAll(siblings);
    }

    public void addSibling(Node sibling) {
        this.siblings.add(sibling);
    }

    @Override
    public String toString() {
        return "value: " + this.value + ", siblings: " + this.siblings.size();
    }
}

class WayData {

    private int currentRoute = -1;
    private int transfers = -1;

    public WayData() {
    }

    public WayData(WayData data) {
        this.currentRoute = data.currentRoute;
        this.transfers = data.transfers;
    }

    public WayData(int transfers, int route) {
        this.currentRoute = route;
        this.transfers = transfers;
    }

    public int getCurrentRoute() {
        return currentRoute;
    }

    public void setCurrentRoute(int currentRoute) {
        this.currentRoute = currentRoute;
    }

    public int getTransfers() {
        return transfers;
    }

    public void setTransfers(int transfers) {
        this.transfers = transfers;
    }

    public void incTransfers() {
        this.transfers += 1;
    }

    @Override
    public String toString() {
        return "route: " + this.currentRoute + ", transfers: " + this.transfers;
    }
}

class WayComparer {

    private List<Route> ways = new ArrayList<>();
    private List<Route> routes = new ArrayList<>();

    private List<Integer> paths = new ArrayList<>();

    static {
        // getWays().add(new Route(Arrays.asList(2, 3, 11, 12, 4)));
        // getWays().add(new Route(Arrays.asList(2, 3, 6, 4)));
        // getWays().add(new Route(Arrays.asList(2, 1, 6, 4)));
        //
        // getRoutes().add(new Route(Arrays.asList(1, 2, 3, 11, 12, 4)));
        // getRoutes().add(new Route(Arrays.asList(5, 6, 4)));
        // getRoutes().add(new Route(Arrays.asList(1, 6, 7)));
        // getRoutes().add(new Route(Arrays.asList(8, 6, 3)));
        // ways.add(Arrays.asList(1, 2, 3, 4, 6, 7));
        // ways.add(Arrays.asList(1, 2, 3, 11, 16, 15, 14, 10, 13, 7));
        //
        // routes.add(Arrays.asList(1, 2, 3, 4));
        // routes.add(Arrays.asList(5, 6, 4));
        // routes.add(Arrays.asList(9, 6, 7));
        // routes.add(Arrays.asList(12, 1, 2, 3, 11, 16, 15, 14, 10, 13, 7));
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public List<Route> getWays() {
        return ways;
    }

    public void setWays(List<Route> ways) {
        this.ways = ways;
    }

    public void solve() {
        for (Route way : getWays()) {
            findPath(way, 0, new WayData());
            // System.out.println(way);
            // System.out.println(0);
        }
        if (paths.size() > 0) {
            Collections.sort(paths);
            System.out.println(paths.get(0));
        } else {
            System.out.println("None");
        }
    }

    private void findPath(Route way, int wayInd, WayData data) {
        for (int i = 0; i < getRoutes().size(); i++) {
            int routeInd = getRoutes().get(i).getStops().indexOf(way.getStops().get(wayInd));
            if (isAppropriate(way, wayInd, getRoutes().get(i), routeInd)) {
                WayData newData = new WayData(data);
                if (data.getCurrentRoute() != i) {
                    newData.incTransfers();
                    newData.setCurrentRoute(i);
                }
                if (wayInd == way.getStops().size() - 2) {
                    scoreResult(way, newData);
                } else {
                    findPath(way, wayInd + 1, newData);
                }
            }
        }
    }

    private void scoreResult(Route way, WayData data) {
        paths.add(data.getTransfers() * 12 + (way.getStops().size() - 1) * 7);
    }

    private boolean isAppropriate(Route way, int wayInd, Route route, int routeInd) {
        if (routeInd < 0) {
            return false;
        }
        if (routeInd < route.getStops().size() - 1 && route.getStops().get(routeInd + 1).equals(way.getStops().get(wayInd + 1))) {
            return true;
        }
        if (routeInd > 0 && route.getStops().get(routeInd - 1).equals(way.getStops().get(wayInd + 1))) {
            return true;
        }
        return false;
    }
}
