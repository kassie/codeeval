package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class NGram {

    private static final String BASE = "Mary had a little lamb its fleece was white as snow; " +
            "And everywhere that Mary went, the lamb was sure to go. " +
            "It followed her to school one day, which was against the rule; " +
            "It made the children laugh and play, to see a lamb at school. " +
            "And so the teacher turned it out, but still it lingered near, " +
            "And waited patiently about till Mary did appear. " +
            "\"Why does the lamb love Mary so?\" the eager children cry; \"Why, Mary " +
            "loves the lamb, you know\" the teacher did reply.\"";

    private static List<String> dict = new ArrayList<>();

    static {
        for (String s : BASE.split(" ")) {
            dict.add(removeNonAlphaNumeric(s));
        }
    }

    private void solve(BufferedReader bufferedReader, PrintStream out) throws IOException {
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] params = line.split(",");
            int n = Integer.valueOf(params[0]);
            String phrase = params[1];

            List<String> options = findOptions(n, phrase);
            if (options.isEmpty()) {
                continue;
            }
            Map<String, Double> countMap = getCountMap(options);

            ValueComparator comparator = new ValueComparator(countMap);
            Map<String, Double> sortedCountMap = new TreeMap<>(comparator);
            sortedCountMap.putAll(countMap);

            NumberFormat formatter = new DecimalFormat("#0.000");
            StringBuilder sb = new StringBuilder();
            for (Entry<String, Double> e : sortedCountMap.entrySet()) {
                sb.append(e.getKey());
                sb.append(",");
                sb.append(formatter.format(e.getValue()));
                sb.append(";");
            }

            out.write(prepareString(sb).getBytes());
        }
    }

    private String prepareString(StringBuilder sb) {
        String s = sb.toString();
        int length = sb.toString().length();
        if (length > 2) {
            s = s.substring(0, length - 1);
        }
        return s + "\n";
    }

    private Map<String, Double> getCountMap(List<String> options) {
        Map<String, Double> countMap = new HashMap<>();
        if (options.size() == 1) {
            countMap.put(options.get(0), (double) 1);
            return countMap;
        }
        String prev = options.get(0);
        int count = 1;
        int total = options.size();
        for (int i = 1; i < options.size(); i++) {
            String s = options.get(i);
            if (s.equals(prev)) {
                count++;
            } else {
                countMap.put(prev, (double) count / total);
                prev = s;
                count = 1;
            }
        }
        countMap.put(prev, (double) count / total);
        return countMap;
    }

    private List<String> findOptions(int n, String phrase) {
        List<String> options = new ArrayList<>();
        String[] words = phrase.split(" ");
        for (int i = 0; i < dict.size() - 1; i++) {
            if (words[0].equals(dict.get(i))) {
                boolean flag = true;
                for (int wordNum = 1; wordNum < words.length; wordNum++) {
                    if (!words[wordNum].equals(dict.get(i + wordNum))) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    options.add(dict.get(i + n - 1));
                }
            }
        }
        Collections.sort(options);
        return options;
    }

    private static String removeNonAlphaNumeric(String s) {
        return s.replaceAll("[^0-9a-zA-Z]", "");
    }

    public static void main(String[] args) throws IOException {
        new NGram().solve(new BufferedReader(new FileReader("samples/ngram.txt")), System.out);
    }

    public class ValueComparator implements Comparator<String> {
        Map<String, Double> base;

        public ValueComparator(Map<String, Double> base) {
            this.base = base;
        }

        public int compare(String a, String b) {
            if (base.get(a) > base.get(b)) {
                return -1;
            } else if (base.get(a) < base.get(b)) {
                return 1;
            } else {
                return a.compareTo(b);
            }
        }
    }
}
