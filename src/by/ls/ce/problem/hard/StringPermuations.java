package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class StringPermuations {

    public static void solve(BufferedReader in) throws Exception {
        String line;
        while ((line = in.readLine()) != null) {
            printPermuations(line);
        }
    }

    private static void printPermuations(String word) {
        List<Character> chars = new LinkedList<Character>();
        for (Character c : word.toCharArray()) {
            chars.add(c);
        }
        List<String> words = new ArrayList<String>();
        for (List<Character> line : permuate(chars)) {
            StringBuilder sb = new StringBuilder();
            for (char c : line) {
                sb.append(c);
            }
            words.add(sb.toString());
        }
        Collections.sort(words);
        StringBuilder sb = new StringBuilder();
        for (String s : words) {
            sb.append(s);
            sb.append(",");
        }
        System.out.println(sb.toString().substring(0, sb.toString().length() - 1));
    }

    private static List<List<Character>> permuate(List<Character> word) {
        List<List<Character>> result = new ArrayList<>();
        if (word.size() == 1) {
            return Arrays.asList(word);
        }
        if (word.size() == 2) {
            result.add(word);
            result.add(Arrays.asList(word.get(1), word.get(0)));
            return result;
        }
        for (int i = 0; i < word.size(); i++) {
            List<Character> incompleteWord = new ArrayList<Character>();
            for (char c : word) {
                incompleteWord.add(c);
            }
            incompleteWord.remove(i);
            for (List<Character> c : permuate(incompleteWord)) {
                List<Character> charCombinations = new ArrayList<>();
                charCombinations.add(word.get(i));
                charCombinations.addAll(c);
                result.add(charCombinations);
            }
        }
        return result;
    }
}
