package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// TODO not solved
public class Labyrinth {

    private static final char WAY = ' ';
    private static final char WALL = '*';
    private static final char PATH = '+';

    private static Map<Integer, Character> charMap = new HashMap<>();

    static {
        charMap.put(0, WAY);
        charMap.put(1, WALL);
        charMap.put(2, PATH);
    }

    private List<List<Integer>> labyrinth = new ArrayList<List<Integer>>();
    private Set<Set<Point>> paths = Collections.synchronizedSet(new HashSet<Set<Point>>());

    public void solve(BufferedReader in, OutputStream os) throws Exception {
        long start = System.currentTimeMillis();
        labyrinth = parseLabrnth(in);
        Point entrance = findEntrance();
        Point next = new Point(entrance.getX(), entrance.getY() + 1);
        Set<Point> path = new HashSet<>();
        path.add(entrance);
        path.add(next);
        Thread t = new PathFinder(entrance, next, path);
        t.start();
        t.join();
        System.out.println(System.currentTimeMillis() - start);
        insertPath(selectPath());
        print(os);
    }

    private void insertPath(Set<Point> path) {
        for (Point p : path) {
            setValue(p, 2);
        }
    }

    private Set<Point> selectPath() {
        int current = Integer.MAX_VALUE;
        Set<Point> shortest = null;
        for (Set<Point> way : paths) {
            if (way.size() < current) {
                current = way.size();
                shortest = way;
            }
        }
        return shortest;
    }

    private void print(OutputStream os) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (List<Integer> row : labyrinth) {
            for (Integer i : row) {
                sb.append(charMap.get(i));
            }
            sb.append("\r\n");
        }
        os.write(sb.toString().trim().getBytes());
    }

    // private void checkPoint(Point prev, Point current, Set<Point> path) {
    // List<Point> ways = getFreeWays(getDirections(current, prev), path);
    // if (ways.isEmpty()) {
    // return;
    // } else {
    // for (Point p : ways) {
    // if (p.getY() == labyrinth.size() - 1) {
    // path.add(p);
    // paths.add(path);
    // return;
    // }
    //
    // Set<Point> newPath = new HashSet<>(path);
    // newPath.add(p);
    // checkPoint(current, p, newPath);
    // }
    // }
    //
    // }

    private Set<Point> getFreeWays(Set<Point> directions, Set<Point> path) {
        Iterator<Point> iterator = directions.iterator();
        while (iterator.hasNext()) {
            Point p = iterator.next();
            if (p.getY() == labyrinth.size()
                    || getValue(p) == 1
                    || path.contains(p)) {
                iterator.remove();
            }
        }
        return directions;
    }

    private Set<Point> getDirections(Point current, Point prev) {
        Set<Point> ways = new HashSet<>();
        ways.add(new Point(current.getX() + 1, current.getY()));
        ways.add(new Point(current.getX() - 1, current.getY()));
        ways.add(new Point(current.getX(), current.getY() + 1));
        ways.add(new Point(current.getX(), current.getY() - 1));
        ways.remove(prev);
        return ways;
    }

    private List<List<Integer>> parseLabrnth(BufferedReader in) throws Exception {
        String line;
        while ((line = in.readLine()) != null) {
            List<Integer> row = new ArrayList<Integer>();
            for (char c : line.toCharArray()) {
                if (c == WALL) {
                    row.add(1);
                } else {
                    row.add(0);
                }
            }
            labyrinth.add(row);
        }
        return labyrinth;
    }

    private Point findEntrance() {
        int startRow = 0;
        for (int cell = 0; cell < labyrinth.get(startRow).size(); cell++) {
            if (labyrinth.get(startRow).get(cell) == 0) {
                return new Point(cell, startRow);
            }
        }
        return null;
    }

    private int getValue(Point p) {
        return labyrinth.get(p.getY()).get(p.getX());
    }

    private void setValue(Point p, int value) {
        labyrinth.get(p.getY()).set(p.getX(), value);
    }

    class PathFinder extends Thread {

        private Point prev;
        private Point current;
        private Set<Point> path;
        private int iteration = 0;
        private int iterationLimit = 200;

        public PathFinder(Point prev, Point current, Set<Point> path) {
            this.prev = prev;
            this.current = current;
            this.path = path;
        }

        @Override
        public void run() {
            checkPoint(prev, current, path);
        }

        private void checkPoint(Point prev, Point current, Set<Point> path) {
            Set<Point> ways = getFreeWays(getDirections(current, prev), path);
            if (ways.isEmpty()) {
                return;
            } else {

                if (ways.size() > 1 && ++iteration > iterationLimit) {
                    for (Point p : ways) {
                        if (!path.add(p) || p.getY() == labyrinth.size() - 1) {
                            paths.add(path);
                            return;
                        }

                        Set<Point> newPath = new HashSet<>(path);
                        newPath.add(p);
                        new PathFinder(current, p, newPath);
                    }
                    iteration = 0;
                } else {
                    for (Point p : ways) {
                        if (!path.add(p) || p.getY() == labyrinth.size() - 1) {
                            paths.add(path);
                            return;
                        }

                        Set<Point> newPath = new HashSet<>(path);
                        newPath.add(p);
                        checkPoint(current, p, newPath);
                    }
                }
            }

        }
    }

}

class Point implements Comparable<Point> {

    private final int x;
    private final int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Point other = (Point) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

    @Override
    public int compareTo(Point p) {
        if (p.getY() > this.y)
            return -1;
        if (p.getY() < this.y)
            return 1;
        return 0;
    }

}
