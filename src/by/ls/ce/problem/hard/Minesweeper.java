package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class Minesweeper {

    private static final int INT_MINE = 9;
    private static final char CHAR_MINE = '*';

    private void solve(BufferedReader bufferedReader, PrintStream out) throws IOException {
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            String[] params = line.split(";");
            int rows = Integer.parseInt(params[0].split(",")[0]);
            int cols = Integer.parseInt(params[0].split(",")[1]);
            int[][] field = parseField(params, rows, cols);
            int[][] result = setMines(rows, cols, field);

            out.write(convertFieldToString(rows, cols, result).getBytes());
        }
    }

    private String convertFieldToString(int rows, int cols, int[][] result) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                if (result[i][j] >= INT_MINE) {
                    sb.append(CHAR_MINE);
                } else {
                    sb.append(result[i][j]);
                }
            }
        }
        sb.append("\n");
        return sb.toString();
    }

    private int[][] setMines(int rows, int cols, int[][] field) {
        int[][] result = new int[rows + 2][cols + 2];
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                if (field[i - 1][j - 1] == 1) {
                    incMineCounter(result, i, j);
                }
            }
        }
        return result;
    }

    private int[][] parseField(String[] params, int rows, int cols) {
        int[][] field = new int[rows][cols];
        int currentRow = 0;
        int currentCol = 0;
        char[] charField = params[1].trim().toCharArray();
        for (char c : charField) {
            if (currentCol == cols) {
                currentCol = 0;
                currentRow++;
            }
            if (c == CHAR_MINE) {
                field[currentRow][currentCol] = 1;
            } else {
                field[currentRow][currentCol] = 0;
            }
            currentCol++;
        }
        return field;
    }

    private void incMineCounter(int[][] array, int row, int col) {
        array[row][col] = INT_MINE;

        array[row - 1][col]++;
        array[row - 1][col + 1]++;
        array[row][col + 1]++;
        array[row + 1][col + 1]++;
        array[row + 1][col]++;
        array[row + 1][col - 1]++;
        array[row][col - 1]++;
        array[row - 1][col - 1]++;
    }

    public static void main(String[] args) throws IOException {
        new Minesweeper().solve(new BufferedReader(new FileReader("samples/minesweeper.txt")), System.out);
    }

}
