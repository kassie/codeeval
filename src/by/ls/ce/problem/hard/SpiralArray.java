package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

public class SpiralArray {

    private void solve(BufferedReader in, OutputStream os) throws IOException {
        String line = null;
        while ((line = in.readLine()) != null) {
            String[] params = line.split(";");
            int rows = Integer.parseInt(params[0]);
            int cols = Integer.parseInt(params[1]);
            String[][] array = new String[rows][cols];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    array[i][j] = params[2].trim().split(" ")[i * cols + j];
                }
            }
            int rowsBackward = rows - 1;
            int rowsForward = 0;
            int colsForward = 0;
            int colsBackward = cols - 1;

            StringBuilder sb = new StringBuilder();
            while (rowsForward <= rowsBackward && colsForward <= colsBackward) {
                for (int i = colsForward; i <= colsBackward; i++) {
                    sb.append(array[rowsForward][i] + " ");
                }
                rowsForward++;
                for (int i = rowsForward; i <= rowsBackward; i++) {
                    sb.append(array[i][colsBackward] + " ");
                }
                colsBackward--;
                for (int i = colsBackward; i >= colsForward && rowsForward <= rowsBackward; i--) {
                    sb.append(array[rowsBackward][i] + " ");
                }
                rowsBackward--;
                for (int i = rowsBackward; i >= rowsForward && colsForward <= colsBackward; i--) {
                    sb.append(array[i][colsForward] + " ");
                }
                colsForward++;
            }
            os.write((rows + "x" + cols + ":" + sb.toString() + "\n").getBytes());
        }
    }

    public static void main(String[] args) throws IOException {
        new SpiralArray().solve(new BufferedReader(new FileReader("samples/spiral.txt")), System.out);
    }
}
