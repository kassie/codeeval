package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// TODO not solved
public class Levenshtein {

    private void solve(BufferedReader bufferedReader, PrintStream out) throws IOException {
        List<String> dictionary = new ArrayList<>();
        List<String> targets = new ArrayList<>();
        parseInput(bufferedReader, dictionary, targets);

        for (String target : targets) {
            Set<String> network = new HashSet<>();
            for (String word : dictionary) {
                if (levenshtein(target, word) == 1) {
                    network.add(word);
                    getFriends(dictionary, network, word, target);
                }
            }
            System.out.println(network.size());
        }
    }

    private void getFriends(List<String> dictionary, Set<String> network, String word, String prevTarget) {
        for (String word2 : dictionary) {
            if (levenshtein(word, word2) == 1 && network.add(word2)) {
                getFriends(dictionary, network, word2, word);
            }
        }
    }

    private int levenshtein(String w1, String w2) {
        int[][] matrix = new int[w1.length() + 1][w2.length() + 1];
        for (int i = 1; i <= w1.length(); i++) {
            matrix[i][0] = i;
        }

        for (int j = 1; j <= w2.length(); j++) {
            matrix[0][j] = j;
        }

        for (int j = 1; j <= w2.length(); j++) {
            for (int i = 1; i <= w1.length(); i++) {
                if (w1.charAt(i - 1) == w2.charAt(j - 1)) {
                    matrix[i][j] = matrix[i - 1][j - 1];
                } else {
                    matrix[i][j] = Math.min(Math.min(matrix[i - 1][j], matrix[i][j - 1]),
                            matrix[i - 1][j - 1]) + 1;
                }
            }
        }
        return matrix[w1.length()][w2.length()];
    }

    private void parseInput(BufferedReader bufferedReader, List<String> dictionary, List<String> words) throws IOException {
        String line;
        boolean inputEnded = false;
        while ((line = bufferedReader.readLine()) != null) {
            if (inputEnded) {
                dictionary.add(line);
            } else {
                if (line.equals("END OF INPUT")) {
                    inputEnded = true;
                    continue;
                }
                words.add(line);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new Levenshtein().solve(new BufferedReader(new FileReader("samples/levenshtein.txt")), System.out);
    }

}
