package by.ls.ce.problem.hard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Scyscrapers {

    private static final int HEIGHT_PARAM = 1;
    private static final int ENDING_INDEX_PARAM = 2;
    private static final int BEGINNING_INDEX_PARAM = 0;

    private void solve(BufferedReader in, OutputStream os) throws IOException {
        String line;
        while ((line = in.readLine()) != null) {
            os.write(formatLanscapeString(line).getBytes());
        }
    }

    private String formatLanscapeString(String line) {
        String landscapeString = getLandscapeString(line).replaceAll(",", "");
        return landscapeString.substring(1, landscapeString.length() - 1) + "\n";
    }

    private String getLandscapeString(String line) {
        List<Block> landscape = getLandscape(line);
        List<Integer> heightsLine = getHeightsLine(landscape);
        List<Integer> landscapeSeq = new ArrayList<>();
        int prevHeight = -1;
        int shift = 1;
        for (int height : heightsLine) {
            if (prevHeight < 0) {
                prevHeight = height;
                landscapeSeq.add(shift);
            } else if (prevHeight != height) {
                landscapeSeq.add(height);
                landscapeSeq.add(shift);
                prevHeight = height;
            } else if (prevHeight == height) {
                incrementLastElement(landscapeSeq);
            }
            shift++;
        }
        landscapeSeq.remove(landscapeSeq.size() - 1);
        return landscapeSeq.toString();
    }

    private void incrementLastElement(List<Integer> landscapeSeq) {
        int lastElementIndex = landscapeSeq.size() - 1;
        landscapeSeq.set(lastElementIndex, landscapeSeq.get(lastElementIndex) + 1);
    }

    private List<Integer> getHeightsLine(List<Block> landscape) {
        List<Integer> heights = new ArrayList<>();
        int lastBlock = landscape.get(landscape.size() - 1).getX();
        for (int shift = 1; shift <= lastBlock + 1; shift++) {
            int height = 0;
            Iterator<Block> iterator = landscape.iterator();
            while (iterator.hasNext()) {
                int nextShift = iterator.next().getX();
                if (shift == nextShift) {
                    height++;
                    iterator.remove();
                } else {
                    break;
                }
            }
            heights.add(height);
        }
        return heights;
    }

    private List<Block> getLandscape(String line) {
        List<Block> landscape = new LinkedList<>();
        for (String building : line.split(";")) {
            createLandscape(landscape, building);
        }
        Collections.sort(landscape);
        makeUnique(landscape);
        return landscape;
    }

    private void makeUnique(List<Block> landscape) {
        Block current = null;
        Iterator<Block> iterator = landscape.iterator();
        while (iterator.hasNext()) {
            Block next = iterator.next();
            if (next.equals(current)) {
                iterator.remove();
            } else {
                current = next;
            }
        }
    }

    private void createLandscape(List<Block> landscape, String building) {
        int[] params = parseBuilding(building);
        for (int i = params[BEGINNING_INDEX_PARAM] + 1; i <= params[ENDING_INDEX_PARAM]; i++) {
            for (int j = 1; j <= params[HEIGHT_PARAM]; j++) {
                landscape.add(new Block(i, j));
            }
        }
    }

    private int[] parseBuilding(String building) {
        int[] intBuilding = new int[3];
        int counter = 0;
        for (String s : building.split(",")) {
            intBuilding[counter++] = Integer.parseInt(exctractDigit(s));
        }
        return intBuilding;
    }

    private String exctractDigit(String s) {
        return s.replaceAll("[()]", "").trim();
    }

    public static void main(String[] args) throws IOException {
        new Scyscrapers().solve(new BufferedReader(new FileReader("samples/scyscrapers.txt")), System.out);
    }
}

class Block implements Comparable<Block> {
    private int x;
    private int y;

    Block(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Block other = (Block) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

    @Override
    public int compareTo(Block another) {
        if (this.x > another.x) {
            return 1;
        } else if (this.x < another.x) {
            return -1;
        } else if (this.y > another.y) {
            return 1;
        } else if (this.y < another.y) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
}